---
- name: Check if GNOME installed-tests testing harness is installed
  register: gnome_desktop_testing_runner
  find:
      paths: "{{ ansible_env.PATH.split(':') }}"
      pattern: gnome-desktop-testing-runner

- name: Build and install GNOME installed-tests testing harness
  when: gnome_desktop_testing_runner.matched == 0
  block:
    - name: Installing build dependencies for IBus and GNOME installed-tests testing harness
      package:
          name:
            - git
            - make
            - gcc
            - diffutils
            - autoconf
            - automake
            - libtool
            - glib2-devel
            - systemd-devel
            - gnome-session
            - gnome-shell
            - dbus-x11
            - xorg-x11-server-Xvfb
            - ibus
            - ibus-desktop-testing
            - ibus-tests
            # ibus-compose test needs locales
            - glibc-langpack-el
            - glibc-langpack-fi
            - glibc-langpack-pt

    - name: Fetching GNOME installed-tests testing harness source from remote repository
      git:
          repo: 'https://gitlab.gnome.org/GNOME/gnome-desktop-testing.git'
          dest: gnome-desktop-testing
          force: yes

    - name: Checkout v2021.1 tag in GNOME installed-tests testing harness
      command: git checkout -b v2021.1 refs/tags/v2021.1
      args:
          chdir: gnome-desktop-testing

    - name: Configure GNOME installed-tests testing harness build
      command: ./autogen.sh --prefix=/usr --sysconfdir=/etc --localstatedir=/var
      args:
          chdir: gnome-desktop-testing

    - name: Build GNOME installed-tests testing harness
      command: make
      args:
          chdir: gnome-desktop-testing

    - name: Install GNOME installed-tests testing harness
      command: make install
      args:
          chdir: gnome-desktop-testing

- name: Start IBus installed-tests testing harness
  environment:
    ANSIBLE: 1
    TMPDIR: '{{ remote_artifacts }}'
    G_MESSAGES_DEBUG: 'all'
    LANG: 'C.UTF-8'
  block:
  - name: Execute IBus tests
    shell: |
      set -e
      # Delete LC_CTYPE=C.UTF-8
      export -n LC_CTYPE
      status="FAIL: frame"
      ibus-desktop-testing-runner \
          --no-graphics \
          --runner=gnome \
          --tests='{{ installed_test_name }}' \
          --output='{{ remote_artifacts }}/{{ installed_test_name }}.log' \
          --result='{{ remote_artifacts }}/test.log' \
          null
      if [ $? -eq 0 ]; then
          status="PASS: frame"
      fi
      echo "${status}" >> {{ remote_artifacts }}/test.log
      echo "#### {{ remote_artifacts }}/{{ installed_test_name }}.log"
      if [ -f {{ remote_artifacts }}/{{ installed_test_name }}.log ] ; then
          cat {{ remote_artifacts }}/{{ installed_test_name }}.log
      fi
      echo "#"
      echo "#### {{ remote_artifacts }}/test.log"
      if [ -f {{ remote_artifacts }}/test.log ] ; then
          cat {{ remote_artifacts }}/test.log
      fi
      echo "#"

  - name: Check the results
    shell: |
        IS_RAWHIDE=`grep -i rawhide /etc/fedora-release`
        if [ x"$IS_RAWHIDE" != x ] ; then
            exit 0
        fi
        log="{{ remote_artifacts }}/test.log"
        if [ ! -f $log ] ; then
            echo ERROR
        else
            FAIL=`grep "^FAIL: " $log | grep -v 'FAIL: 0$'`
            if [ x"$FAIL" != x ] ; then
                echo ERROR
            fi
        fi
    register: test_fails
    failed_when: False

  - name: Set role result
    set_fact:
      role_result: "{{ test_fails.stdout }}"
      role_result_failed: "{{ (test_fails.stdout|d|length > 0) or (test_fails.stderr|d|length > 0) }}"
      role_result_msg: "{{ test_fails.stdout|d('tests failed.') }}"

  - include_role:
      name: str-common-final

